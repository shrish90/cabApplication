package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
public class BookingEntity {
	@Id
	private int  BOOKINGID ;
	private String   CABID;
	private String  USERID;
	private String  BOOKINGSTATUS;
	public String getCABID() {
		return CABID;
	}
	public void setCABID(String cABID) {
		CABID = cABID;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getBOOKINGSTATUS() {
		return BOOKINGSTATUS;
	}
	public void setBOOKINGSTATUS(String bOOKINGSTATUS) {
		BOOKINGSTATUS = bOOKINGSTATUS;
	}
	
	  
	
	

}
