package com.example.service.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.BookingDAO;
import com.example.model.BookingModel;
import com.example.service.BookingService;

@Service
public class BookingServiceImpl implements BookingService{
	@Autowired
	BookingDAO bookingDao;
	
	@Override
	public String bookCab(BookingModel booking) {
		String bookingId = "";
		List<Map<String,Object>> allCabs = bookingDao.getAllCabs();
		Double currentUserLong = Double.parseDouble(booking.getUserLocationLong());
		Double currentUserLat = Double.parseDouble(booking.getUserLocationLat());
		//Double nearestCab = 0.0;
		Map<Double,String> avaliableCab = new TreeMap<Double,String>();
		for(Map<String,Object> eachCab: allCabs) {
			double distanceinKM = distance(currentUserLat,currentUserLong,Double.parseDouble(eachCab.get("LOCLAT")+""),Double.parseDouble(eachCab.get("LOCLONG")+""),"K");
			if(distanceinKM <= 50) {
				avaliableCab.put(distanceinKM,eachCab.get("CABID")+"");
			}
		}
		for(Double kms: avaliableCab.keySet()) {
			Map<String,Object> cabBooking = new HashMap<String,Object>();
			cabBooking.put("CABID",avaliableCab.get(kms));
			cabBooking.put("USERID",booking.getUserId());
			cabBooking.put("BOOKINGSTATUS","Booked");
			boolean isCabBooked = isCabBooked(avaliableCab.get(kms));
			if(!isCabBooked) {
				bookingId = bookingDao.bookCab(cabBooking);
				break;
			}else {
				continue;
			}
			
		}
		return bookingId;
	}
	private boolean isCabBooked(String cabId) {
		return bookingDao.isCabBookedAlready(cabId);
	}
	private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		}
		return (dist);
	}
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
