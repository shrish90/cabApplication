package com.example.service;

import com.example.model.BookingModel;

public interface BookingService {
	public String bookCab(BookingModel booking);
}
