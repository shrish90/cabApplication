package com.example.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.BookingModel;
import com.example.service.BookingService;

@RestController
@RequestMapping("/book")
public class BookingController {
	@Autowired
	BookingService bookingService;
	@RequestMapping(value="/cab", method = RequestMethod.POST)
	public ResponseEntity<String> bookCab(@Valid @RequestBody BookingModel booking) {
		String bookingID = bookingService.bookCab(booking);
		return new ResponseEntity<>(bookingID,HttpStatus.CREATED);
	}
}
