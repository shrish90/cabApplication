package com.example.dao;

import java.util.List;
import java.util.Map;


public interface BookingDAO {
	public String bookCab(Map<String,Object> booking);
	public boolean isCabBookedAlready(String cabId);
	public List<Map<String,Object>> getAllCabs();
}
