package com.example.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.example.entity.BookingEntity;



@Service
public interface CancleDAO extends Repository<BookingEntity, Long>,CrudRepository<BookingEntity, Long>{
	

		
		@Modifying(clearAutomatically = true)
		@Query("update BOOKING booking set BOOKINGSTATUS  =:isCancled where bookingId =:bookingId")
		void cancleBooking(@Param("bookingId") int bookingId, @Param("isCancled") boolean isCancled);
	
}
