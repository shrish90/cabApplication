package com.example.dao.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.example.dao.BookingDAO;
import com.example.model.BookingModel;

@Repository
public class BookingDAOImpl implements BookingDAO{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String ALLCAB_SQL = "SELECT CABID,CABNUMBER,LOCLONG,LOCLAT FROM CABS";
	private static final String CAB_SQL_CABID = "SELECT BOOKINGID FROM BOOKING WHERE CABID=?";
	@Override
	public String bookCab(Map<String,Object> booking) {
		String bookingId = "";
		SimpleJdbcInsert simpleInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("booking").usingGeneratedKeyColumns("BOOKINGID");
		bookingId = simpleInsert.executeAndReturnKey(booking).longValue()+"";
		return bookingId;
	}

	@Override
	public List<Map<String,Object>> getAllCabs() {
		List<Map<String,Object>> cabDetails = new ArrayList<Map<String,Object>>();
		cabDetails = jdbcTemplate.queryForList(ALLCAB_SQL);
		return cabDetails;
	}

	@Override
	public boolean isCabBookedAlready(String cabId) {
		boolean isCabBooked = false;
		List<Map<String,Object>> cabDetails = new ArrayList<Map<String,Object>>();
		cabDetails = jdbcTemplate.queryForList(CAB_SQL_CABID,cabId);
		if(!cabDetails.isEmpty()) {
			isCabBooked = true;
		}
		return isCabBooked;
	}

}
