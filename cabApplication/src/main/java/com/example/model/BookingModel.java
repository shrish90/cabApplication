package com.example.model;

import javax.validation.constraints.NotNull;

public class BookingModel {
	
	@NotNull
	String userId;
	@NotNull
	String userLocationLat;
	@NotNull
	String userLocationLong;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserLocationLat() {
		return userLocationLat;
	}
	public void setUserLocationLat(String userLocationLat) {
		this.userLocationLat = userLocationLat;
	}
	public String getUserLocationLong() {
		return userLocationLong;
	}
	public void setUserLocationLong(String userLocationLong) {
		this.userLocationLong = userLocationLong;
	}
	
	
}
